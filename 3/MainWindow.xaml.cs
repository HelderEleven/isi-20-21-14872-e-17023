﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.IO;

namespace _3
{
    /// <summary>
    /// Data logic.
    /// </summary>
    public static class Data
    {
        public static Dictionary<string, object> DataGlobal { get; set; }
        public static ObservableCollection<Regiao> DataRegiao { get; set; }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var httpClient = new HttpClient();
            var response = httpClient.GetAsync("https://api.ipma.pt/open-data/distrits-islands.json").Result;
            var regioes = response.Content.ReadAsStringAsync().Result;

            Data.DataGlobal = JsonConvert.DeserializeObject<Dictionary<string, object>>(regioes);
            Data.DataRegiao = JsonConvert.DeserializeObject<ObservableCollection<Regiao>>(Data.DataGlobal["data"].ToString());

            foreach (var regiao in Data.DataRegiao)
                regiao.GerarDetalhes();

            selecionarRegiao.ItemsSource = Data.DataRegiao;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var selected = selecionarRegiao.SelectedItem as Regiao;
            var _previsaoWrapPanel = painel.previsaoWrapPanel;

            if (selected != null)
            {
                if (_previsaoWrapPanel.Children.Count > 0)
                    _previsaoWrapPanel.Children.Clear();

                
                double ampMin = -300;
                double ampMax = -300;
                foreach (var dia in selected.Detalhes)
                {
                    var previsao = new Design.DiaPrevisao();
                    previsao.dialabel.Content = dia.forecastDate.ToShortDateString();
                    previsao.minlabel.Content = dia.tMin + "º";
                    previsao.maxlabel.Content = dia.tMax + "º";
                    previsao.ventolabel.Content = dia.predWindDir;
                    previsao.humidadeLabel.Content = dia.precipitaProb.Split('.')[0] + "%";
                    string name = Assembly.GetEntryAssembly().GetName().Name;
                    int precipitaPropInt = Convert.ToInt32(dia.precipitaProb.Split('.')[0]);
                    if (precipitaPropInt <= 50)
                        previsao.humidadeImagem.Source = new BitmapImage(new Uri("/3;Component/Resources/gotaAgua0.png", UriKind.Relative));
                    else
                        previsao.humidadeImagem.Source = new BitmapImage(new Uri("/3;Component/Resources/gotaAgua50.png", UriKind.Relative));
                    previsao.imagemVento.Source = new BitmapImage(new Uri("/3;Component/Resources/orientacaoVento-" + dia.predWindDir + ".png", UriKind.Relative));
                    string nomeImagem = "";
                    switch (dia.idWeatherType)
                    {
                        case int n when (n == 1):
                            nomeImagem = "tempo1.png";
                            break;

                        case int n when (n >= 2 && n <= 7):
                            nomeImagem = "tempo2-7.png";
                            break;
                        case int n when (n >= 8 && n <= 15):
                            nomeImagem = "tempo8-15.png";
                            break;
                    }
                    previsao.imagemPrevisao.Source = new BitmapImage(new Uri("/3;Component/Resources/" + nomeImagem, UriKind.Relative));

                    var tmax = Convert.ToDouble(dia.tMax.Replace('.', ','));
                    var tmin = Convert.ToDouble(dia.tMin.Replace('.', ','));

                    if (ampMin == -300)
                        ampMin = tmin;
                    else if (ampMin > tmin)
                        ampMin = tmin;
                    if (ampMax == -300)
                        ampMax = tmax;
                    else if (ampMax < tmax)
                        ampMax = tmax;

                    previsao.mediaLabel.Content = Math.Round((Convert.ToDouble(dia.tMax.Replace('.', ','))
                                                  + Convert.ToDouble(dia.tMin.Replace('.', ','))) / 2, 1).ToString() + "º";
                    amplitudetb.Text = ampMin.ToString().Replace(',','.') + " e " + ampMax.ToString().Replace(',', '.');
                    _previsaoWrapPanel.Children.Add(previsao);
                }
                btnJson.IsEnabled = true;
            }
            else btnJson.IsEnabled = false;
        }

        private void btnJson_Click(object sender, RoutedEventArgs e)
        {
            Regiao regiaoSelected = (selecionarRegiao.SelectedItem as Regiao);
            foreach (var detalhes in (selecionarRegiao.SelectedItem as Regiao).Detalhes)
            {
                detalhes.temperatura = new Temperatura
                {
                    tMin = Convert.ToDouble(detalhes.tMin.Replace('.', ',')),
                    tMax = Convert.ToDouble(detalhes.tMax.Replace('.', ',')),
                    tMed = Convert.ToDouble(
                        Math.Round((Convert.ToDouble(detalhes.tMax.Replace('.', ','))
                                  + Convert.ToDouble(detalhes.tMin.Replace('.', ','))) / 2, 1)
                        )
                };

            }
            string json = JsonConvert.SerializeObject(regiaoSelected, Formatting.Indented);
            File.WriteAllText(Directory.GetCurrentDirectory() + "\\output\\json\\" + regiaoSelected.local + ".json", json);
            MessageBox.Show("Ficheiro gerado com sucesso em: " + Directory.GetCurrentDirectory() + "\\output\\json\\" + regiaoSelected.local + ".json");
        }

        private void btnJsonDias_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, Dictionary<string, List<Detalhes>>> megaEstrutura = new Dictionary<string, Dictionary<string, List<Detalhes>>>();
            for (int i = 0; i < 5; i++)
            {
                string diaAtualShort = DateTime.Today.AddDays(i).ToShortDateString();

                Dictionary<string, List<Detalhes>> detalhesDia = (from regiao in Data.DataRegiao
                                                          select new KeyValuePair<string, List<Detalhes>>(
                                                            regiao.local, // Key
                                                            regiao.Detalhes.Where(d => // Value
                                                                d.forecastDate.ToShortDateString() == diaAtualShort).ToList()
                                                            )).ToDictionary(kp => kp.Key, kp => kp.Value);
                megaEstrutura.Add(diaAtualShort, detalhesDia);
            }
            foreach(var diasFileWrite in megaEstrutura)
            {
                string json = JsonConvert.SerializeObject(diasFileWrite, Formatting.Indented);
                File.WriteAllText(Directory.GetCurrentDirectory() + "\\output\\dias\\" + diasFileWrite.Key.Replace('/', '-') +
                    (diasFileWrite.Key == DateTime.Today.ToShortDateString() ? "-hoje" : "") + ".json", json);
            }
            MessageBox.Show("Ficheiros gerados em " + Directory.GetCurrentDirectory() + "\\output\\dias");
        }
    }
}
