﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace _3
{
    public class Regiao
    {
        public Regiao()
        {
        }
        public int idRegiao { get; set; }
        public string idAreaAviso { get; set; }
        public int idConcelho { get; set; }
        public int globalIdLocal { get; set; }
        public string latitude { get; set; }
        public int idDistrito { get; set; }
        public string local { get; set; }
        public string longitude { get; set; }
        public List<Detalhes> Detalhes { get; set; }

        public void GerarDetalhes()
        {
            var link = "https://api.ipma.pt/open-data/forecast/meteorology/cities/daily/" + globalIdLocal + ".json";
            var httpClient = new HttpClient();
            var response = httpClient.GetAsync(link).Result;
            var info = response.Content.ReadAsStringAsync().Result;

            var _tmpData = JsonConvert.DeserializeObject<Dictionary<string, object>>(info);
            Detalhes = JsonConvert.DeserializeObject<List<Detalhes>>(_tmpData["data"].ToString());
        }
    }
}
