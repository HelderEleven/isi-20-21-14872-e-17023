﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3
{
    public class Temperatura
    {
        public double tMin { get; set; }
        public double tMax { get; set; }
        public double tMed { get; set; }
    }
}
