﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace _3
{
    public class Detalhes
    {
        public string precipitaProb { get; set; }
        public string tMin { get; set; }
        public string tMax { get; set; }
        public string predWindDir { get; set; }
        public int idWeatherType { get; set; }
        public string classWindSpeed { get; set; }
        public string longitude { get; set; }
        public DateTime forecastDate { get; set; }
        public int classPrecInt { get; set; }
        public string latitude { get; set; }
        public Temperatura temperatura { get; set; }
    }
}
