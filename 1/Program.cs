﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.IO;

namespace _1
{
    class Program
    {
        private static async Task<string> getData(string url)
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(url);
            var result = await response.Content.ReadAsStringAsync();

            return result;
        }

        static void Main(string[] args)
        {
            string locais = getData("https://api.ipma.pt/open-data/distrits-islands.json").Result;
            MatchCollection resultadosCodigo = Regex.Matches(locais, "\"globalIdLocal\": ([0-9]{7})");
            MatchCollection resultadosLocal = Regex.Matches(locais, "\"local\": \"([ ]*[A-Z|a-z|\\x5C][a-z0-9|\\x5C]+)+\"");
            for(int i=0; i<resultadosCodigo.Count; i++)
            {
                var codigo = resultadosCodigo[i].Value.Split(':')[1].Trim();
                var link = "https://api.ipma.pt/open-data/forecast/meteorology/cities/daily/" + codigo + ".json";
                var infoLocal = getData(link).Result;
                int index = 0;
                while(infoLocal.ElementAt(index++) != '{');
                infoLocal = infoLocal.Insert(index, resultadosLocal[i].Value + ", ");
                File.WriteAllText(Directory.GetCurrentDirectory()+"\\output\\"+codigo+"-detalhe.json", infoLocal);
            }
            Console.WriteLine("Dados processados.");
            Console.ReadKey();
        }
    }
}
