﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
            string locais = Local.getData("https://api.ipma.pt/open-data/distrits-islands.json").Result;
            Local GestorLocais = new Local(locais);
            int i=0;
            Console.WriteLine("Escolha a opção desejada:\n");
            foreach(string output in GestorLocais.ListaLocais())
            {
                i++;
                Console.WriteLine(i + " - " + output);
            }
            Console.WriteLine("Opção:");

            //while (!int.TryParse(Console.ReadLine(), out opcao) && (opcao > GestorLocais.DataLocais.Count || opcao <= 0));

            int opcaoLocais = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Quer converter os dados em (1) Json ou (2) XML?");

            int opcaoConverter = Convert.ToInt32(Console.ReadLine());

            switch (opcaoConverter)
            {
                case 1:
                    GestorLocais.ConverterJson(opcaoLocais);
                    break;
                case 2:
                    GestorLocais.ConverterXML(opcaoLocais);
                    break;
            }

            Console.ReadKey();
        }
    }
}
