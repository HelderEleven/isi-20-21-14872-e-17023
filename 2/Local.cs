﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace _2
{
    class Local
    {
        public static async Task<string> getData(string url)
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(url);
            var result = await response.Content.ReadAsStringAsync();

            return result;
        }

        Dictionary<string, object> DataGlobal { get; set; }

        
        public List<Dictionary<string, string>> DataLocais { get; set; }


        public Local(string locals)
        {
            DataGlobal = JsonConvert.DeserializeObject<Dictionary<string, object>>(locals);


            DataLocais = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(DataGlobal["data"].ToString());
        }

        public List<string> ListaLocais()
        {
            List<string> listaLocais = new List<string>();
            foreach(var local in DataLocais)
            {
                listaLocais.Add(local["local"]);
            }
            return listaLocais;
        }

        public int ConverterJson(int opcaoLocal)
        {
            var globalIdLocal = DataLocais.ElementAt(opcaoLocal - 1)["globalIdLocal"];
            var link = "https://api.ipma.pt/open-data/forecast/meteorology/cities/daily/" + globalIdLocal + ".json";
            var info = getData(link).Result;

            File.WriteAllText(Directory.GetCurrentDirectory() + "\\output\\json\\" + globalIdLocal + "-detalhe.json", info);

            return 0;
        }

        public int ConverterXML(int opcaoLocal)
        {
            //XDocument documento = new XDocument(
            //    new XElement((XName)"Locais",
            //        (from local
            //         in DataLocais
            //         select new XElement((XName)"Local",
            //            (from info
            //             in local
            //             select new XElement((XName)info.Key, info.Value))
            //         ))
            //    )
            //);



            var globalIdLocal = DataLocais.ElementAt(opcaoLocal - 1)["globalIdLocal"];
            var link = "https://api.ipma.pt/open-data/forecast/meteorology/cities/daily/" + globalIdLocal + ".json";
            var info = getData(link).Result;

            var Data = JsonConvert.DeserializeObject<Dictionary<string, object>>(info);
            var DataRegiao = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(Data["data"].ToString());

            XDocument documento = new XDocument(
                new XElement((XName)DataLocais.ElementAt(opcaoLocal - 1)["local"],
                    (from regiao
                     in DataRegiao
                     select new XElement((XName)"Dia",
                        (from dia
                         in regiao
                         select new XElement((XName)dia.Key, dia.Value)
                     ))
                ))
            );
            documento.Save(Directory.GetCurrentDirectory() + "\\output\\xml\\" + globalIdLocal + "-detalhe.xml");
            return 0;
        }
    }
}
